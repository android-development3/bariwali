# BariWali



### Master Branch

| branch_name |  master |
|-------------|---|
| description |  this is master branch   |
| version     | 0.1.XXX |
| remote_name | gitlab  |
| remote_url  |  https://gitlab.com/android-development3/bariwali.git |
| update      |  null |



### Development Branch 

| branch_name |  development |
|-------------|---|
| description |  this branch is being created for development and testing  |
| version     | 0.1.x.xx |
| remote_name | gitlab  |
| remote_url  |  https://gitlab.com/android-development3/bariwali.git |
| update      |  under development |



## Tracker (Development Branch)

- **Alpha v0.1.1.03**
    - Adding Navigation Drawer  
    - Removed Login and Register from service as experimantal 
    - Removed GridView from MainActivity as experimantal 
    - Bug ~ Fix Scrollbar in NavigationDrawer!  // Fixed 
        - rebuild NavigationDrawer from official doc (typora)



## Bugs and To-Do List 

- Registration and PIN Login 
  
    - *A Possible way solve ~ is add a new table on database* 
    
- Add a navigation drawer bar

- Bottom navigation bar or a tab view on top

- Remove update activity 

- Add a app bar 

    - ![appbar](images/appbar_sheets.png)

    


## Changelog

 * **Alpha 0.1.0.015**
   
        - Git Initiate
        - Working in [development] branch
        - Added **sdp** library
        - Created **MainActivity** layout 
        - Added PIN Checker functionality 
        - Changed class MainActivity to PINLogin
        - Added Register PIN layout
        - Added **GridView** in MainActivity
        - Added Banner Image in MainActivity
        - Added Background Image in MainActivity
        - Added Button Icons 
            - Create 
            - Remove
            - Update
        - Added About 
        - Added  DataBase
        - Added CreateSubject Method! 
        - Added ShowData method
        - Added ListView in ShowData
        - Changing Database Algorithm 
	   - tagged alpha v0.1.0.015
     
* **Alpha 0.1.0.19**

   - Implemented New Algorithm
   - Added show data as alert dialog
   - Moved to old algorithm of database due to update and delete issue 
   - Add data is worked successfully worked!
   - Update data "floor" is worked only
   - Code Bug found! (no update is working....)
   - Applying New SQLite Algorithm
   - Renamed **UpdateSubject** to **UpdateDatabase** 
   - Renamed **update_subject** to **update_database** 
   - Modified and changed layout **update_database** 
   - Added **delete_button** on **update_database**
   - Updated **DatabaseHelper**
   - Added **DatabaseManager** 
   - Added Custom title 
   - tagged alpha v0.1.0.19

- **Alpha v0.1.1.00**
    - Added **unit** on database 
    - Updated Layout
    - Little Code Bug Fixes

   

 * Beta v0.1.xx
 * Stable 1.xx



# Development By, <br>
### **Farhan Sadik**

### *Square Development Group*

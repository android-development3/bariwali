package com.squaredevops.bariwali;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridViewAdapter extends ArrayAdapter {

       /*
        NOT IN SERVICE
     */

    Context context;
    String[] items;
    Integer[] logo;

    // the constructor gets the values to be shown
    // this is a constructor
    public GridViewAdapter(Context context, String[] items, Integer[] logo) {
        super(context, R.layout.gridview_layout, items);
        this.context = context;
        this.items = items;
        this.logo = logo;
    }

    //getview method inflates the values given from the mainactivity on the custom design layout for gridview and returns the layout with inflated values in it
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView= inflater.inflate(R.layout.gridview_layout, null, true);

        //initialize the textview and imageview we declared in the gridview_layout.xml file
        TextView title = (TextView) rowView.findViewById(R.id.text);
        ImageView image = (ImageView) rowView.findViewById(R.id.image);

        title.setText(items[position]);
        image.setImageResource(logo[position]);

        return rowView;
    }

}

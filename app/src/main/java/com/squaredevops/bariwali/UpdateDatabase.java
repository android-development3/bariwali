package com.squaredevops.bariwali;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class UpdateDatabase extends AppCompatActivity implements View.OnClickListener {

    EditText name_EditText, floor_EditText, unit_EditText;
    Button update_button, show_database_button, delete_database_button;
    long _id;
    DatabaseManager databaseManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_database);
        setTitle("Update Database");

        name_EditText = (EditText) findViewById(R.id.name_update);
        floor_EditText = (EditText) findViewById(R.id.floor_update);
        unit_EditText = (EditText) findViewById(R.id.unit_update);

        update_button = (Button) findViewById(R.id.update_database);
        show_database_button = (Button) findViewById(R.id.show_me_from_update);
        delete_database_button = (Button) findViewById(R.id.delete_database);

        databaseManager = new DatabaseManager(this);
        databaseManager.open();

        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        String name = intent.getStringExtra("name");
        String floor = intent.getStringExtra("floor");
        String unit = intent.getStringExtra("unit");

        //_id = Long.parseLong(id);
        name_EditText.setText(name);
        floor_EditText.setText(floor);
        unit_EditText.setText(unit);

        // on click
        /*
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //databaseHelper.update_floor((recent_position+1), floor.getText().toString());
                Toast.makeText(UpdateDatabase.this, "Updated", Toast.LENGTH_SHORT).show();

                boolean isUpdate = databaseHelper.updateData(name.getText().toString(),
                        floor.getText().toString(),
                        unit.getText().toString());
                if(isUpdate)
                    Toast.makeText(UpdateSubject.this,"Data Update",Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(UpdateSubject.this,"Data not Updated",Toast.LENGTH_LONG).show();

            }
        });
        */

        update_button.setOnClickListener(this);
        delete_database_button.setOnClickListener(this);


        // show data
        show_database_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), ShowDatabase.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.update_database:
                String n = name_EditText.getText().toString();
                String f = floor_EditText.getText().toString();
                String u = unit_EditText.getText().toString();

                databaseManager.update(_id, n, f, u);
                this.returnHome();
                break;

            case R.id.delete_database:
                databaseManager.delete(_id);
                this.returnHome();
                break;

        }
    }

    public void returnHome() {
        Intent home_intent = new Intent(getApplicationContext(), ShowDatabase.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(home_intent);
    }
}
package com.squaredevops.bariwali;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PINRegister extends AppCompatActivity {

    EditText name, pin;
    Button register_button;

    /*
        NOT IN SERVICE
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_register);

        name = (EditText) findViewById(R.id.login_name);
        pin = (EditText) findViewById(R.id.login_pin);
        register_button = (Button) findViewById(R.id.register_button);

    }
}
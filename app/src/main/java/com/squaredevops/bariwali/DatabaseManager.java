package com.squaredevops.bariwali;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseManager {

    /*
        Database Manager
     */

    private DatabaseHelper databaseHelper;
    private Context context;
    private SQLiteDatabase database;

    // constructor
    public DatabaseManager(Context c) {
        context = c;
    }

    // open database
    public DatabaseManager open() throws SQLException {
        databaseHelper = new DatabaseHelper(context);
        database = databaseHelper.getWritableDatabase();
        return this;
    }

    // close database
    public void close() {
        databaseHelper.close();
    }

    // insert data to database
    public void insert(String name, String floor, String unit) {
        ContentValues contentValue = new ContentValues();

        contentValue.put(DatabaseHelper.NAME, name);
        contentValue.put(DatabaseHelper.FLOOR, floor);
        contentValue.put(DatabaseHelper.UNIT, unit);

        // return
        database.insert(DatabaseHelper.TABLE_NAME, null, contentValue);
    }


    // fetch
    public Cursor fetch() {

        String[] columns = new String[] { DatabaseHelper._ID, DatabaseHelper.NAME, DatabaseHelper.FLOOR, DatabaseHelper.UNIT };
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, null, null, null, null, null, null);

        // check
        if (cursor != null) {
            cursor.moveToFirst();
        }

        return cursor;
    }

    // update database
    public int update(long _id, String name, String floor, String unit) {

        ContentValues contentValues = new ContentValues();

        contentValues.put(DatabaseHelper.NAME, name);
        contentValues.put(DatabaseHelper.FLOOR, floor);
        contentValues.put(DatabaseHelper.UNIT, unit);

        int intent = database.update(DatabaseHelper.TABLE_NAME, contentValues, DatabaseHelper._ID + " = " + _id, null);
        return intent;
    }

    // delete database
    public void delete(long _id) {
        database.delete(DatabaseHelper.TABLE_NAME, DatabaseHelper._ID + "=" + _id, null);
    }

}

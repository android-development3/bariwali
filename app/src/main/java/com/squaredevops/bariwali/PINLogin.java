package com.squaredevops.bariwali;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PINLogin extends AppCompatActivity {

    EditText pin;
    Button pin_button, register_button;

       /*
        NOT IN SERVICE
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_login);

        pin = (EditText) findViewById(R.id.give_us_pin);
        pin_button = (Button) findViewById(R.id.enter_pin);
        register_button = (Button) findViewById(R.id.register_pin);

        //yourEditText.setText(currentUserName);
        pin.setText("777247");

        pin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // getting pin number as user input
                String pin_number = pin.getText().toString();
                int value = 0;
                if (!"".equals(pin_number)) {
                    value = Integer.parseInt(pin_number);
                }

                int polka = 777247;
                if ( polka == value ) {
                    //Toast.makeText(PINLogin.this, "Pin Matched", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                } else {
                    Toast.makeText(PINLogin.this, "Wrong PIN", Toast.LENGTH_SHORT).show();
                }

            }
        });

        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getApplicationContext(), PINRegister.class));

            }
        });

    }
}
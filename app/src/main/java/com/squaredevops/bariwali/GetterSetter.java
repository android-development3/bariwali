package com.squaredevops.bariwali;

public class GetterSetter {

    /*
        Getter Setter Method
        Not in Service
     */

    private int id;
    private String name;
    private String floor;
    private String unit;

    // constructor
    public GetterSetter(String n, String f, String u) {
        name = n;
        floor = f;
        unit = u;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}

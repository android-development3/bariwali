package com.squaredevops.bariwali;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class ShowDatabase extends AppCompatActivity {

    ListView listView;
    DatabaseManager databaseManager;
    SimpleCursorAdapter simpleCursorAdapter;

    // array
    final String[] data = new String[] {
            DatabaseHelper._ID,
            DatabaseHelper.NAME,
            DatabaseHelper.FLOOR,
            DatabaseHelper.UNIT
    };

    // array
    final int[] to = new int[] {
            R.id.listview_id,
            R.id.listview_title,
            R.id.listview_floor,
            R.id.listview_unit
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_database);
        setTitle("Show Database");

        // Listview layout = list_view_layout
        listView = (ListView) findViewById(R.id.list_view);

        // object
        databaseManager = new DatabaseManager(this);
        databaseManager.open();
        Cursor cursor = databaseManager.fetch();

        // if list view is empty
        listView.setEmptyView(findViewById(R.id.empty));

        // adapter
        simpleCursorAdapter = new SimpleCursorAdapter(this,
                R.layout.listview_layout, // list view layout
                cursor,
                data,  // string from global ver
                to, 0);

        simpleCursorAdapter.notifyDataSetChanged();
        listView.setAdapter(simpleCursorAdapter);

        //listView.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.listview_layout, R.id.show_data, data));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView idTextView = (TextView) view.findViewById(R.id.listview_id);
                TextView titleTextView = (TextView) view.findViewById(R.id.listview_title);
                TextView floorTextView = (TextView) view.findViewById(R.id.listview_floor);
                TextView unitTextView = (TextView) view.findViewById(R.id.listview_unit);

                String idA = idTextView.getText().toString();
                String t = titleTextView.getText().toString();
                String f = floorTextView.getText().toString();
                String u = unitTextView.getText().toString();

                // new activity for update or delete database
                Intent intent = new Intent(getApplicationContext(), UpdateDatabase.class);

                intent.putExtra("title", t);
                intent.putExtra("floor", f);
                intent.putExtra("unit", u);
                intent.putExtra("id", idA);

                startActivity(intent);
            }
        });

    }

}
package com.squaredevops.bariwali;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CreateSubject extends AppCompatActivity implements View.OnClickListener {

    EditText name_EditText;
    EditText floor_EditText;
    EditText unit_EditText;

    Button save_button, show_button;
    DatabaseManager databaseManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_subject);
        setTitle("Create Entry");

        name_EditText = (EditText) findViewById(R.id.name);
        floor_EditText = (EditText) findViewById(R.id.floor);
        unit_EditText = (EditText) findViewById(R.id.unit);

        save_button = (Button) findViewById(R.id.save);
        show_button = (Button) findViewById(R.id.show);

        databaseManager = new DatabaseManager(this);
        databaseManager.open();

        save_button.setOnClickListener(this);

        /*
        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // getting text
                String _name = name_EditText.getText().toString();
                String _floor = floor_EditText.getText().toString();
                String _unit = unit_EditText.getText().toString();

                // creating object of getter setter class
                // adding to table
                GetterSetter getterSetter = new GetterSetter(_name, _floor, _unit); // _name = n and _day = d from getter setter
                dataBase.addDataOnTable(getterSetter);

                // added a toast
                Toast.makeText(getApplicationContext(),"Data Added Successfully", Toast.LENGTH_LONG).show();
            }
        });
        */

        show_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // going for a new activity
                startActivity(new Intent(getApplicationContext(), ShowDatabase.class));
            }
        });
    }

    @Override
    public void onClick(View view) {
        // save = button id
        if (view.getId() == R.id.save) {

            final String name = name_EditText.getText().toString();
            final String floor = floor_EditText.getText().toString();
            final String unit = unit_EditText.getText().toString();

            databaseManager.insert(name, floor, unit);

            Intent main = new Intent(CreateSubject.this, ShowDatabase.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(main);
        }
    }
}
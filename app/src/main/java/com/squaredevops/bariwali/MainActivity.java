package com.squaredevops.bariwali;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    GridView gridView;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;

    /*  those are related to gridview

    // items to show in gridview
    String [] items = {"Create", "Update", "Show List"};

    // those r images from drawable
    Integer [] logo = {R.drawable.create_icon, R.drawable.update_icon, R.drawable.view_icon};

    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* those are related to gridview
        // call the CustomAdapter constructor and pass the values you want to be shown in the gridview
        GridViewAdapter adapter = new GridViewAdapter(MainActivity.this, items, logo);
        gridView = (GridView) findViewById(R.id.grid_view);
        gridView.setAdapter(adapter);

        // setting onclick function
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String itemValue = (String) gridView.getItemAtPosition(position);
                // Toast.makeText(MainActivity.this, itemValue, Toast.LENGTH_SHORT).show();

                // create
                if(position==0){
                    Intent i0 = new Intent(MainActivity.this, CreateSubject.class);
                    startActivity(i0);
                }

                // update
                if(position==1){
                    Intent i1 = new Intent(MainActivity.this, ShowDatabase.class);
                    startActivity(i1);
                }

                // remove
                if(position==2){
                    Intent i2 = new Intent(MainActivity.this, ShowDatabase.class);
                    startActivity(i2);
                }
            }
        });
        */

        // adding navigation drawer
        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // adding toggle button
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }  // onCrete

    // menu on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {


        // enable toggle on click listner
        if (toggle.onOptionsItemSelected(item)) {
            // this is navigation drawer menu
            return true;
        } else if (item.getItemId() == R.id.action_settings) {
            // this is action bar menu
            startActivity(new Intent(getApplicationContext(), About.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Intent intent;

        switch (item.getItemId()) {
            case R.id.menu1:
                Toast.makeText(this, "Clicked Menu 1", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu2:
                Toast.makeText(this, "Clicked Menu 2", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;

        /*

        if (item.getItemId() == R.id.nav_menu_item_1) {
            //intent = new Intent(this, Home.class);
            //startActivity(intent);
            Toast.makeText(MainActivity.this, "Updated", Toast.LENGTH_SHORT).show();
        } else if (item.getItemId() == R.id.nav_menu_item_3) {
            //intent = new Intent(this, Chat.class);
            //startActivity(intent);
            Toast.makeText(getApplicationContext(). "clicked!", Toast.LENGTH_SHORT).show();
        }

        */

        }

        return false;
    }
}
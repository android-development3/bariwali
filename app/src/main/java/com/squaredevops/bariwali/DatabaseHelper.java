package com.squaredevops.bariwali;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseHelper extends SQLiteOpenHelper {

    // database variable
    public static final String DATABASE_NAME = "subject_database.db";
    public static final String TABLE_NAME = "SUBJECTS";

    // Table Columns
    public static final String _ID = "_id";
    public static final String NAME = "NAME";
    public static final String FLOOR = "FLOOR";
    public static final String UNIT = "UNIT";

    // database version
    static final int database_version = 1;

    // constructor
    public DatabaseHelper(Context context) {
        super (context, DATABASE_NAME, null, database_version);
    }

    // Creating table query
    private static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NAME + " TEXT NOT NULL, " + FLOOR + " TEXT, " + UNIT + " TEXT);";
    //+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + NAME + " TEXT NOT NULL, " + FLOOR + " TEXT);";

    // implemented method of SQLiteOpenHelper class
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //sqLiteDatabase.execSQL("create table " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, FLOOR INTEGER, UNIT TEXT)");
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    // implemented method of SQLiteOpenHelper class
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
